package com.gomobi.swapi;

import android.content.Context;
import android.os.Parcel;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.gomobi.swapi.api.ApiUtils;
import com.gomobi.swapi.model.People;
import com.gomobi.swapi.model.Person;

import org.hamcrest.core.IsNull;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)

public class AppTests {

    // Test date formatting
    @Test
    public void dateTest() {
        String dateStr = "2014-12-10T16:16:29.192000Z";
        String readable = ApiUtils.personDisplayDate(dateStr);
        assertFalse(readable.isEmpty());
        assertThat(readable, is("10/12/2014 16:16:29"));
    }

    // Test that the parceable Person object returns it's values correctly.
    @Test
    public void personParcel() {
        Person person = new Person("Luke Skywalker", "77", "172", "2014-12-09T13:50:51.644000Z");
        Parcel parcel = Parcel.obtain();
        person.writeToParcel(parcel, person.describeContents());
        parcel.setDataPosition(0);
        Person personFromParcel = Person.CREATOR.createFromParcel(parcel);
        assertThat(personFromParcel.getName(), is("Luke Skywalker"));
        assertThat(personFromParcel.getMass(), is("77"));
        assertThat(personFromParcel.getHeight(), is("172"));
        assertThat(personFromParcel.getDisplayDate(), is("09/12/2014 13:50:51"));
    }

    // Test converting an input stream to a string
    @Test
    public void streamToString() throws IOException {
        Context ctx = InstrumentationRegistry.getTargetContext();
        InputStream is = ctx.getResources().getAssets().open("sampledata.json");
        assertThat(is, is(IsNull.notNullValue()));
        String testStr = ApiUtils.stringFromStream(is);
        assertThat(testStr, IsNull.notNullValue());
    }

    // Test JSON parsing
    @Test
    public void jSONParse() throws IOException, JSONException {
        Context ctx = InstrumentationRegistry.getTargetContext();
        InputStream is = ctx.getResources().getAssets().open("sampledata.json");
        String jsonStr = ApiUtils.stringFromStream(is);
        assertThat(jsonStr, IsNull.notNullValue());
        JSONObject jsonObject = new JSONObject(jsonStr);
        People people = new People();
        people.append(jsonObject);
        assertThat(people.count(), is(10));
        assertThat(people.getNextUrl(), is("https://swapi.co/api/people/?page=2"));
        Person ls = people.itemAt(0);
        assertThat(ls.getName(), is("Luke Skywalker"));
        Person owk = people.itemAt(9);
        assertThat(owk.getName(), is("Obi-Wan Kenobi"));
        assertThat(people.itemAt(10), IsNull.nullValue());
    }
}
