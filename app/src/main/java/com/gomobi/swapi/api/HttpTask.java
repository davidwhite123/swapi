package com.gomobi.swapi.api;

import android.os.AsyncTask;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

public class HttpTask extends AsyncTask<String, Void, String> {
    private final HttpURLConnection mConnection;
    private final HttpTaskListener mHttpTaskListener;
    private String mMessage;
    private String mResponse;
    private int mResponseCode;
    private boolean mSuccess;

    HttpTask(HttpURLConnection connection, HttpTaskListener listener) {
        mConnection = connection;
        mHttpTaskListener = listener;
    }

    /**
     * Checks that the response code is in the 200 range
     * @return true if code is in range
     */
    private boolean responseCodeOk(){
        return mResponseCode >= 200 && mResponseCode < 300;
    }

    /**
     * Creates a string representation of the http response
     * @throws IOException exception raised if a stream error occurs
     */
    private void getResponse() throws IOException {
        InputStream inStream;
        mResponseCode = mConnection.getResponseCode();
        mSuccess = responseCodeOk();
        // if the response code is not in the range 2xx,
        // getInputStream() will be null.
        // In this case, we use getErrorStream()
        if (mSuccess) {
            inStream = mConnection.getInputStream();
            mResponse = streamToString(inStream);
        } else {
            inStream = mConnection.getErrorStream();
            mMessage = streamToString(inStream);
        }
    }

    /**
     * Creates a string from the given stream
     * @param inputStream the stream received by the request
     * @return string value of the response
     * @throws IOException exception raised if a stream error occurs
     */
    private String streamToString (InputStream inputStream) throws IOException{
        return ApiUtils.stringFromStream(inputStream);
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            getResponse();
            return mSuccess ? mResponse : mMessage;
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (null != mHttpTaskListener) {
            mHttpTaskListener.onTaskComplete(mSuccess, result);
        }
    }

    public interface HttpTaskListener {
        void onTaskComplete(boolean success, String dataStr);
    }
}
