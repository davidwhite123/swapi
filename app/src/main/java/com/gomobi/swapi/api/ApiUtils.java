package com.gomobi.swapi.api;

import android.content.Context;
import android.net.NetworkInfo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ApiUtils {
    private static final int READ_TIMEOUT      = 10000;
    private static final String BASE_URL       = "https://swapi.co/api/";
    public static final String PEOPLE_ENDPOINT = BASE_URL + "people";

    /**
     * Parse a date string and return a readable formatted date as string
     * @param dateStr - the original date string
     * @return - readable date string
     */
    public static String personDisplayDate(String dateStr) {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'", Locale.UK);
            SimpleDateFormat displayDf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.UK);
            Date date = df.parse(dateStr);
            return displayDf.format(date);
        } catch (ParseException e) {
            return "";
        }
    }

    /**
     * Creates a HttpURLConnection according to the given parameters
     * @param url the url for the connection
     * @return a HttpURLConnection
     * @throws IOException exception raised by a malformed url or openConnection error
     */
    private static HttpURLConnection openRESTConnection(String url) throws IOException {
        URL address = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) address.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoInput(true);
        connection.setReadTimeout(READ_TIMEOUT);
        return connection;
    }


    /**
     * Creates an encoded string from the given string
     * @param inputStream the stream to convert
     * @return UTF8 encoded string
     * @throws IOException exception raised by a stream read error
     */
    public static String stringFromStream(InputStream inputStream) throws IOException {
        if (inputStream == null){
            return null;
        }
        byte[] streamData;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] data = new byte[4096];
        int count;
        while ((count = inputStream.read(data)) != -1){
            bos.write(data, 0, count);
        }
        bos.flush();
        bos.close();
        inputStream.close();
        streamData = bos.toByteArray();
        if (streamData == null){
            return "";
        }
        return new String(streamData, "UTF-8");
    }

    /**
     * Check if we have any sort of Internet connection.
     * @param context - a valid Context
     * @return - return true if Internet available
     */
    public static boolean isConnectedToInternet(Context context){
        android.net.ConnectivityManager connMgr = (android.net.ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null == connMgr) {
            return false;
        }
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private static HttpURLConnection getPeopleConnection(String url) throws IOException {
        return openRESTConnection(url);
    }

    private static void continueRequest(HttpURLConnection connection, HttpTask.HttpTaskListener listener) {
        HttpTask task = new HttpTask(connection, listener);
        task.execute();
    }

    /**
     * Try to make the http request, returning true if an error has occured
     * @param url - the string url
     * @param listener - the HttpTaskListener
     * @return - true if an error occurred
     */
    public static boolean makePeopleRequest_HasError(String url, HttpTask.HttpTaskListener listener) {
        try {
            HttpURLConnection connection = getPeopleConnection(url);
            continueRequest(connection, listener);
            return false;
        } catch (IOException e) {
            return true;
        }
    }
}
