package com.gomobi.swapi.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.gomobi.swapi.R;
import com.gomobi.swapi.model.People;
import com.gomobi.swapi.model.Person;

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.PersonView> {
    private People mPeople;
    private final View.OnClickListener mRowClickListener;

    public PersonAdapter(People people, View.OnClickListener listener) {
        mPeople = people;
        mRowClickListener = listener;
    }

    public void update(People people) {
        mPeople = people;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PersonView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_person, parent, false);
        return new PersonView(itemView);
    }

    @Override
    public int getItemCount() {
        return null == mPeople ? 0 : mPeople.count();
    }

    @Override
    public void onBindViewHolder(@NonNull PersonView holder, int position) {
        Person person = mPeople.itemAt(position);
        holder.lblName.setText(person.getName());
        holder.itemView.setOnClickListener(mRowClickListener);
        holder.itemView.setTag(person);
    }


    class PersonView extends RecyclerView.ViewHolder {
        final TextView lblName;

        PersonView(View view) {
            super(view);
            lblName = view.findViewById(R.id.lblName);
        }

    }
}
