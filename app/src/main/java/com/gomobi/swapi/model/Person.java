package com.gomobi.swapi.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.gomobi.swapi.api.ApiUtils;

import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Person implements Parcelable {
    private String mName;
    private String mMass;
    private String mHeight;
    private String mCreated;

    private Person() {}

    public Person(String name, String mass, String height, String created) {
        mName = name;
        mMass = mass;
        mHeight = height;
        mCreated = created;
    }

    public static Person fromJson(JSONObject jsonObject) throws JSONException {
        Person result = new Person();
        result.mName = jsonObject.getString("name");
        result.mHeight = jsonObject.getString("height");
        result.mMass = jsonObject.getString("mass");
        result.mCreated = jsonObject.getString("created");
        return result;
    }

    public String getName() {
        return mName;
    }

    public String getMass() {
        return mMass;
    }

    public String getHeight() {
        return mHeight;
    }

    public String getDisplayDate() {
        return ApiUtils.personDisplayDate(mCreated);
    }

    //region Parcelable
    private Person(Parcel in) {
        mName = in.readString();
        mMass = in.readString();
        mHeight = in.readString();
        mCreated = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int size) {
        parcel.writeString(mName);
        parcel.writeString(mMass);
        parcel.writeString(mHeight);
        parcel.writeString(mCreated);
    }

    public static final Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>() {

        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };
    //endregion
}
