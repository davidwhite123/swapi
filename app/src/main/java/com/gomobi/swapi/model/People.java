package com.gomobi.swapi.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class People {
    private String mNextUrl;
    private List<Person> mPersonList;

    public String getNextUrl() {
        return mNextUrl;
    }

    public int count() {
        return (mPersonList == null ? 0 : mPersonList.size());
    }

    public Person itemAt(int index) {
        int count = count();
        return (count == 0 || index >= count) ? null : mPersonList.get(index);
    }

    /**
     * Append a page of data to the persons list
     * @param jsonObject - the page as a json object
     * @throws JSONException - throws an exception if there is a parsing error
     */
    public void append(JSONObject jsonObject) throws JSONException {
        mNextUrl = jsonObject.isNull("next") ? null : jsonObject.getString("next");
        JSONArray people = jsonObject.getJSONArray("results");
        for (int idx=0; idx<people.length(); idx++) {
            Person person = Person.fromJson(people.getJSONObject(idx));
            if (person != null) {
                if (null == mPersonList) {
                    mPersonList = new ArrayList<>();
                }
                mPersonList.add(person);
            }
        }
    }
}
