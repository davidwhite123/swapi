package com.gomobi.swapi;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.gomobi.swapi.adapters.PersonAdapter;
import com.gomobi.swapi.api.ApiUtils;
import com.gomobi.swapi.api.HttpTask;
import com.gomobi.swapi.model.People;
import com.gomobi.swapi.model.Person;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private boolean mLoading;
    private People mPeople;
    private ProgressDialog mProgressDialog;
    private PersonAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initiateUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mLoading) {
            startPersonRequest();
        }
    }

    //region UI
    private void initiateUI() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        RecyclerView recyclerView = findViewById(R.id.lsPeople);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new PersonAdapter(mPeople, personListener);
        recyclerView.setAdapter(mAdapter);
    }

    private final View.OnClickListener personListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            Person person = (Person)view.getTag();
            showPerson(person);
        }
    };

    private void showPerson(Person person) {
        startActivity(DetailActivity.getIntent(this, person));
    }
    //endregion

    //region Progress Bar/Dialog
    // Note:
    // ProgressDialog has been deprecated as of Api level 26+ (Oreo)
    @SuppressWarnings("deprecation")
    private void showProgress() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle(getResources().getString(R.string.loading));
        mProgressDialog.setMessage(getResources().getString(R.string.loading_message));
        mProgressDialog.setCancelable(false);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.show();
    }

    private void hideProgress() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog.dismiss();
                mAdapter.update(mPeople);
            }
        });
    }
    //endregion

    //region Error Message
    private void showError(final int titleResId, final int messageResId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(titleResId);
                builder.setMessage(messageResId);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setCancelable(false);
                builder.show();
            }
        });

    }
    //end region

    //region REST call
    private void showNoInternet() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.alert_title);
        builder.setMessage(R.string.alert_message);
        builder.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startPersonRequest();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void startPersonRequest() {
        if (!ApiUtils.isConnectedToInternet(this)) {
            showNoInternet();
            return;
        }
        initiatePersonRequest();
    }

    private void initiatePersonRequest() {
        mLoading = true;
        showProgress();
        if (ApiUtils.makePeopleRequest_HasError(ApiUtils.PEOPLE_ENDPOINT, peopleListener)) {
            hideProgress();
        }
    }

    private void continuePersonRequest(String url) {
        if (ApiUtils.makePeopleRequest_HasError(url, peopleListener)) {
            hideProgress();
        }
    }

    private void parsePeople(String dataStr) throws JSONException {
        JSONObject jsonObject = new JSONObject(dataStr);
        mPeople.append(jsonObject);
    }

    private final HttpTask.HttpTaskListener peopleListener = new HttpTask.HttpTaskListener() {
        @Override
        public void onTaskComplete(boolean success, String dataStr) {
            if (success) {
                if (null == mPeople) {
                    mPeople = new People();
                }
                try {
                    parsePeople(dataStr);
                    if (null != mPeople.getNextUrl()) {
                        continuePersonRequest(mPeople.getNextUrl());
                    } else {
                        hideProgress();
                    }
                } catch (JSONException e) {
                    hideProgress();
                    showError(R.string.json_error_title, R.string.json_error_message);
                }
            } else {
                hideProgress();
                showError(R.string.http_error_title, R.string.http_error_message);
            }
        }
    };
    //endregion
}
