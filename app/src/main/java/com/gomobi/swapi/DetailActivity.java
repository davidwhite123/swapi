package com.gomobi.swapi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.gomobi.swapi.model.Person;

public class DetailActivity extends AppCompatActivity {
    private static final String PERSON_KEY = "person";

    public static Intent getIntent(Context context, Person person) {
        Intent intent = new Intent();
        intent.setClass(context, DetailActivity.class);
        intent.putExtra(PERSON_KEY, person);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initiateUI();
    }

    private void initiateUI() {
        Intent intent = getIntent();
        Person person = intent.getParcelableExtra(PERSON_KEY);
        if (null == person) {
            return;
        }
        Toolbar toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setTitle(person.getName());
            }
        }
        populateData(person);
    }

    private void populateData(Person person) {
        TextView tv = findViewById(R.id.detail_Name);
        tv.setText(person.getName());
        tv = findViewById(R.id.detail_Mass);
        tv.setText(person.getMass());
        tv = findViewById(R.id.detail_Height);
        tv.setText(person.getHeight());
        tv = findViewById(R.id.detail_Created);
        tv.setText(person.getDisplayDate());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
