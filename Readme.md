# Readme
The app has been restricted to portrait only and does not include support for split screen, in landscape mode, on tablet devices.

#Environment
The app was built using Android Studio 3.1.3
This means that views found by "findViewById(int resId)" no longer need casting to their respective types.
# Source Code
The method "showProgress" in MainActivity.java has been annotated with a deprecation warning suppression.

This is because the ProgressDialog class used was deprected in Android Oreo, Api level 26+.  Due to time constraints, I have used the deprecated class.
##Test Scope
The app has been tested on the following apis:


* Api level 19 (Physical device)
* Api level 23 (Emulator)
* Api level 25 (Emulator)
* Api level 27 (Emulator) 

